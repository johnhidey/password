defmodule PasswordGenerator do
  @moduledoc """
  Documentation for `PasswordGenerator`.
  """
  @defaults [
    defaults: [
      enabled: true,
      characters:
        Enum.join([
          ?a..?z |> Enum.reduce("", &(&2 <> <<&1>>)),
          ?A..?Z |> Enum.reduce("", &(&2 <> <<&1>>)),
          "!@#$%^&*()_+-=,.<>/?[]}{\\|'\";:`~",
          0..9 |> Enum.reduce("", &(&2 <> to_string(&1)))
        ])
        |> String.split("")
        |> Enum.shuffle()
        |> Enum.join()
    ]
  ]

  @spec generate(keyword(), non_neg_integer()) ::
          {:ok, String.t()}
          | {:error, String.t()}
  def generate(options \\ @defaults, length) do
    with {:ok, options} <- validate_options(options),
         {:ok, options} <- apply_option_defaults(options),
         {:ok, filtered_options} <- filter_enabled_options(options),
         {:ok, characters} <- get_password_characters(filtered_options, length) do
      build_password(characters)
    else
      {:error, message} ->
        {:error, message}
    end
  end

  @spec generate!(keyword(), non_neg_integer()) :: String.t()
  def generate!(options \\ @defaults, length) do
    case generate(options, length) do
      {:ok, password} ->
        password

      {:error, message} ->
        raise ArgumentError, message: message
    end
  end

  defp apply_option_defaults([] = options), do: {:ok, Keyword.merge(@defaults, options)}
  defp apply_option_defaults(options), do: {:ok, options}

  defp filter_enabled_options(options) do
    options
    |> Keyword.filter(fn
      {_, [{:enabled, true} | _]} -> true
      _ -> false
    end)
    |> then(fn value -> {:ok, value} end)
  end

  defp validate_options(options) when is_nil(options) do
    option = "anbled"
    {:error, "#{option} is not a valid configuration option"}
  end

  defp validate_options(options) do
    {:ok, options}
  end

  defp build_password(characters) do
    {:ok,
     characters
     |> List.flatten()
     |> Enum.shuffle()
     |> Enum.join()}
  end

  defp get_password_characters(options, length) do
    options
    |> then(fn filtered_options ->
      filtered_options
      |> Enum.map(fn {_, value} when is_list(value) -> Map.new(value) end)
      |> Enum.map(fn %{characters: characters} -> characters end)
      |> Enum.with_index()
      |> Enum.map(&get_character_counts(&1, Enum.count(filtered_options), length))
      |> Enum.map(fn {characters, count} -> get_random_characters(characters, count) end)
    end)
    |> then(fn value -> {:ok, value} end)
  end

  defp get_character_counts({characters, index}, character_type_count, length) do
    count =
      if index + 1 == character_type_count,
        do: div(length, character_type_count) + rem(length, character_type_count),
        else: div(length, character_type_count)

    {characters, count}
  end

  defp get_random_characters(characters, length) do
    Stream.resource(
      fn -> :noop end,
      fn acc -> {[Enum.random(String.split(characters, "", trim: true))], acc} end,
      fn _ -> :ok end
    )
    |> Enum.take(length)
  end
end

# Password Generator

A simple password generator for gererating those simple passwords as well as very complex passwords.  Passwords out of the box are are a combination of lowercase, uppercase, numbers, and symbols making for a strong password.


## Usage

Out of the book usage is straight forward

```elixir
> # Generate a password using on the default options and the length specified
> PasswordGenerator.generate(10)
{:ok, "?|MJ-iF7E4"}
```

To specify custom options you create a character set to be used.  To create a character set, just give it a name followed by the allowed characters for that set and wether it is enabled or not.

```elixir
> # Specify the `Options` to be used when generating a password
> options = [my_symbol_set: [enabled: true, characters: "&^%$"]]
[my_symbol_set: [enabled: true, characters: "&^%$"]]
> # Generate a new password with the supplied options created above
> PasswordGenerator.generate(options, 10)
{:ok, "^^$&%$$$$$"}
>
> # Multiple character sets are how passwords percentage of use is control.  Say
> # that you want a password made of a to z and numbers 0 to 9.  You also want to
> # know that either is roughly an equal distribution of letters and numbers. 
> options = [letters: [enabled: true, characters: "abcdefghijklmnopqrstuvwxyz"], numbers: [enabled: true, characters: "0123456789"]]
[
  letters: [enabled: true, characters: "abcdefghijklmnopqrstuvwxyz"],
  numbers: [enabled: true, characters: "0123456789"]
]        
> PasswordGenerator.generate(options, 10)
{:ok, "xgs68ku597"}
```



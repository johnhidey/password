defmodule PasswordGeneratorTest do
  use ExUnit.Case, async: false

  @valid_options ~W(
    length
    allow_lowercase
    allow_uppercase
    allow_numbers
    allow_symbols
    lowercase_characters
    uppercase_characters
    number_characters
    symbol_characters)a

  setup do
    default_options = %{
      length: 5,
      allow_lowercase: true,
      allow_uppercase: true,
      allow_symbols: true,
      allow_numbers: true,
      lowercase_characters: "abc",
      uppercase_characters: "ABC",
      number_characters: "123",
      symbol_characters: "!@#"
    }

    %{options: default_options}
  end

  test "Only allow for supported options", %{options: options} do
    invalid_options = %{
      howmany: 5,
      lowercase: true,
      takes_uppercase: true
    }

    for key <- Map.keys(options), do: assert(key in @valid_options)
    for key <- Map.keys(invalid_options), do: refute(key in @valid_options)
  end

  # test "At least 1 of the options of numbers, mixcase, and symbols must be true"
  test "Option length must be an integer", %{options: options} do
    tuples = [{5, true}, {10, true}, {"", false}, {"ten", false}, {3.14, false}]

    Enum.each(tuples, fn {length, result} ->
      options = %{options | length: length}
      # assert(PasswordGenerator.generate(options) == result)
    end)
  end

  # test "Option length must be greater than 0 and less than 200"
  # test "Options for numbers, mixcase, and symbols must all be booleans"
end
